# coding: utf-8

"""树莓派操作"""

import os
import platform


def dpms_on():
    """树莓派显示屏唤醒"""
    try:
        systems = {
            "Darwin": "",
            "Linux": "xset dpms force on"
        }
        system = platform.system()
        cmd = systems.get(system, "")
        if cmd:
            os.system(cmd)
    except:
        pass
